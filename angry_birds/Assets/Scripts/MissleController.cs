﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissleController : MonoBehaviour {

    [SerializeField]
    float force=20f;
    Rigidbody2D rb;
    bool isCurrent = true;

    public bool IsCurrent
    {
        get
        {
            return isCurrent;
        }
        private set
        {
            isCurrent = value;
        }
    }

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezePositionY;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag =="Ground")
        {
            GameController.Instance.StartAimingPhase();
        }
    }

    public void ShootMissle(Vector2 direction)
    {
        isCurrent = false;
        rb.constraints = RigidbodyConstraints2D.None;
        rb.AddForce(direction * force,ForceMode2D.Impulse);
        GameController.Instance.StartShootPhase(transform);
    }
}
