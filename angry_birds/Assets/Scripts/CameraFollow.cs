﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {


    [SerializeField]
    Transform target;
    [SerializeField]
    float smoothing=.2f;

    public static CameraFollow Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    public void SetTarget(Transform newTarget)
    {
        target = newTarget;
    }

    // Update is called once per frame
    void FixedUpdate ()
    {  
        Vector2 newPos = Vector2.Lerp(transform.position, target.position, smoothing);
        transform.position = new Vector3(newPos.x, newPos.y, transform.position.z);
	}
}
