﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {

    [SerializeField]
    float waitTime = 1;

    public static GameController Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }
    
    public void StartShootPhase(Transform missle)
    {
        CameraFollow.Instance.SetTarget(missle);
    }

    public void StartAimingPhase()
    {
        StartCoroutine(AimingPhase());
    }

    IEnumerator AimingPhase()
    {
        yield return new WaitForSeconds(waitTime);
        SlingshotController.Instance.StartAimPhase();
        CameraFollow.Instance.SetTarget(SlingshotController.Instance.SpawnPos);
    }
}
