﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlingshotController : MonoBehaviour {

    [SerializeField]
    float radius=2f;
    [SerializeField]
    GameObject misslePref;
    [SerializeField]
    Transform spawnPos;

    GameObject currMissle;

    public Transform SpawnPos
    {
        get
        {
            return spawnPos;
        }
    }
    public bool canAim = true;
    public static SlingshotController Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    // Use this for initialization
    void Start () {
        SpawnMissle();
	}
	
	// Update is called once per frame
	void Update () {
        if (canAim && Input.GetMouseButtonDown(0))
        {
            CheckForMissle();
        }
    }

 
    public void StartAimPhase()
    {
        canAim = true;
        SpawnMissle();
    }

    void SpawnMissle()
    {
        currMissle = Instantiate(misslePref);
        currMissle.transform.position = spawnPos.position;
    }
    void CheckForMissle()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
        RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
        if (hit ==true)
        {
            MissleController mc = hit.collider.GetComponent<MissleController>();
            if (mc != null && mc.IsCurrent)
            {
                currMissle = mc.gameObject;
                StartCoroutine(Aiming());
            }
        }
    }

    IEnumerator Aiming()
    {
        Vector3 mousePos;
        Vector2 mousePos2D;
        while (Input.GetMouseButton(0))
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos2D = new Vector2(mousePos.x, mousePos.y);
            currMissle.transform.position = mousePos2D;
            if (Vector2.Distance(currMissle.transform.position, spawnPos.position) > radius)
            {
                currMissle.transform.position = spawnPos.position + (currMissle.transform.position- spawnPos.position ).normalized * radius;
            }
            yield return null;
        }
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos2D = new Vector2(mousePos.x, mousePos.y);
        Vector2 direction = spawnPos.position - currMissle.transform.position;

        canAim = false;
        currMissle.GetComponent<MissleController>().ShootMissle(direction);      
    }
}
