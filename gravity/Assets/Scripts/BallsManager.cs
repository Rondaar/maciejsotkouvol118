﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallsManager : MonoBehaviour {


    [SerializeField]
    float timeToSpawn = .25f;
    [SerializeField]
    int explosionAmount = 50;
    [SerializeField]
    float explosionForce = 10;


    public static BallsManager Instance { get; set; }


    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        StartCoroutine(SpawningBalls());
    }

    void SpawnBall()
    {
        float x = Random.Range(0f, 1f);
        float y = Random.Range(0f, 1f);

        Vector2 pos = new Vector2(x, y);
        pos = Camera.main.ViewportToWorldPoint(pos);

        GameObject newObject = ObjectPooler.Instance.GetFromPool();
        newObject.transform.position = new Vector3(pos.x,pos.y,0);
        newObject.GetComponentInChildren<BallController>().SetCollision(true);
    }

    IEnumerator SpawningBalls()
    {
        SpawnBall();
        yield return new WaitForSeconds(timeToSpawn);
        StartCoroutine(SpawningBalls());
    }


    public void Explosion(Vector3 position)
    {
        for(int i = 0; i<explosionAmount;i++)
        {
            GameObject newBall = ObjectPooler.Instance.GetFromPool();
            newBall.transform.position = position;
            Vector2 dir = Random.insideUnitCircle.normalized;
            newBall.GetComponentInChildren<BallController>().SetInvincibleDelayed();
            newBall.GetComponent<Rigidbody2D>().AddForce(dir * explosionForce,ForceMode2D.Impulse);
            
        }
    }
}
