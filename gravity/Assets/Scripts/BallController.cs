﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

    public bool isInvincible = true;

    Rigidbody2D rb;
    Collider2D[] colliders;

    private void Awake()
    {
        rb = GetComponentInParent<Rigidbody2D>();
        colliders = transform.parent.GetComponentsInChildren<Collider2D>();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        BallController bc = collision.GetComponent<BallController>();
        if(bc!=null && isInvincible)
        {
            bc.isInvincible = false;
            
            ContactPoint2D[] contact = new ContactPoint2D[1];
            collision.GetContacts(contact);
            transform.parent.position = contact[0].point;
            transform.parent.localScale = transform.parent.localScale + bc.transform.parent.localScale;
            rb.mass = rb.mass + bc.transform.GetComponentInParent<Rigidbody2D>().mass;
            ObjectPooler.Instance.AddToPool(bc.transform.parent.gameObject);
            CheckForExplosion();
        }
    }

    public void CheckForExplosion()
    {
        if (rb.mass >=50)
        {  
            BallsManager.Instance.Explosion(transform.position);
            ObjectPooler.Instance.AddToPool(transform.parent.gameObject);
        }
    }

    public void SetInvincibleDelayed()
    {
        StartCoroutine(InvincibleMode());
    }

    public void SetCollision(bool b)
    {
        foreach (Collider2D col in colliders)
        {
            col.enabled = b;
        }
    }
    IEnumerator InvincibleMode()
    {
        foreach (Collider2D col in colliders)
        {
            col.enabled = false;
        }
        yield return new WaitForSeconds(.5f);
        foreach (Collider2D col in colliders)
        {
            col.enabled = true;
        }
    }
}
