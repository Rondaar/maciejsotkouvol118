﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectPooler : MonoBehaviour {

    [SerializeField]
    GameObject prefab;
    [SerializeField]
    int poolInitSize = 250;
    [SerializeField]
    Vector3 initScale;

    [SerializeField]
    Text text;
    public int ActiveAmount { get; set; }
    public static ObjectPooler Instance { get; set; }
    private Queue<GameObject> objects = new Queue<GameObject>();

    private void Awake()
    {
        if(Instance !=null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
        ActiveAmount = poolInitSize;
        GrowPool();
    }

    public void AddToPool(GameObject newObject)
    {
        ActiveAmount--;
        SetAmountText();
        newObject.SetActive(false);
        newObject.GetComponent<Rigidbody2D>().mass = 1;
        newObject.transform.localScale = initScale;
        newObject.GetComponentInChildren<BallController>().SetCollision(false);
        objects.Enqueue(newObject);
    }

    public GameObject GetFromPool()
    {
        ActiveAmount++;
        SetAmountText();
        GameObject returnObj = objects.Dequeue();
        returnObj.SetActive(true);
        return returnObj;
    }

    void SetAmountText()
    {
        text.text = "Balls Amount " + ActiveAmount;
    }
    void GrowPool()
    {
        for (int i = 0; i < poolInitSize; i++)
        {
            GameObject instance = Instantiate(prefab);
            AddToPool(instance);
        }
    }

}
